import { IsNotEmpty, IsPositive } from 'class-validator';
class CretedOrderItemDto {
  productId: number;
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;

  @IsNotEmpty()
  orderItems: CretedOrderItemDto[];
}
