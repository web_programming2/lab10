import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find();
  }

  async findOne(id: number) {
    const customer = await this.productsRepository.findOne({
      where: { id: id },
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const customer = await this.productsRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...customer, ...updateProductDto };
    return this.productsRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const customer = await this.productsRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.productsRepository.remove(customer);
  }
}
